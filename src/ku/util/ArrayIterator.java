package ku.util;
import java.util.Iterator;
import java.util.NoSuchElementException;
/**
 * For check an array by ignore null element.
 * @author Salilthip 5710546640
 *
 * @param <T>
 */
public class ArrayIterator<T> implements Iterator<T> {
	private T[] array;
	private int elementNow=0;

	/**
	 * Construct ArrayIerator for run.
	 * @param T is an array that will run
	 */
	public ArrayIterator(T[] T) {
		array=T;
	}

	@Override
	/**
	 * Check the next element is not null.
	 * @return true , if it is not null element.
	 * 		false , if it's null element.
	 */
	public boolean hasNext() {
		for(int i=elementNow;i<array.length;i++){
			if(array[i]!=null)
				return true;
		}
		return false;

	}

	@Override
	/**
	 * Find the next non-null element
	 * @return the information that can find
	 */
	public T next() throws NoSuchElementException{
		while(hasNext()){ 
			if(array[elementNow]==null) elementNow++;
			else if(array[elementNow]!=null) return array[elementNow++];

		}
		throw new NoSuchElementException();
	}


	@Override
	/**
	 * Remove the present element.
	 */
	public void remove(){
		array[elementNow-1]=null;
	}
}
