package ku.util;
import java.util.EmptyStackException;

/**
 * For object that can remove or add by one way.
 * @author Salilthip 5710546640
 *
 * @param <T>
 */
public class Stack<T>{
	private T[] items;
	private int peak=0;

	/**
	 * Initial capacity of stack
	 * @param capacity
	 */
	public Stack(int capacity){
		if(capacity<=0)
			throw new EmptyStackException();
		items = (T[]) new Object[capacity];
		peak =0;
	}
	
	/**
	 * Get the capacity of Stack
	 * @return the capacity
	 */
	public int capacity(){
		if(items.length>=0)
		return items.length;
		else
		return -1;
	}
	
	/**
	 * Check if it is empty.
	 * @return true, if it's empty
	 * 		false, if it isn't empty.
	 */
	public boolean isEmpty(){
		return peak<=0;
	}
	
	/**
	 * Check if it is full.
	 * @return true, if it's full
	 * 		false, if it isn't full.
	 */
	public boolean isFull(){
		return peak==capacity();
	}
	
	/**
	 * Find the top thing of Stack.
	 * @return
	 */
	public T peek(){
		if(isEmpty())
			return null;
		return items[peak-1];
	}
	
	/**
	 * Get the top thing of Stack out.
	 * @return the top thing that will be got out
	 */
	public T pop(){
		if(isEmpty())
			throw new EmptyStackException();
		peak--;
		T result = items[peak];
		items[peak] = null;
		return result;
	}
	
	/**
	 * Add something to the top of stack.
	 * @param obj is the object that will add.
	 */
	public void push(T obj){
		 if (obj==null)
             throw new IllegalArgumentException();
		 if(!isFull()){
		 items[peak++] = obj;
		 }
	}
	
	/**
	 * Get the size of this Stack
	 * @return the size of this stack.
	 */
	public int size(){
		int sum=0;
		for(int i=0;i<items.length;i++){
			if(items[i]!=null)
			sum++;
		}
		return sum;
	}
	
}
