package ku.util;

public class test {
	public static void main(String[] args){
		String [] fruit = { "apple", null, null, "apple"};
		ArrayIterator<String> it = new ArrayIterator(fruit);
		System.out.println(it.hasNext());
		System.out.println(it.next());
		it.remove();
		System.out.println(it.next());
		it.remove();
//		System.out.println(it.hasNext());
//		Object [ ] array = new Object[1];
//		// array containing null
//		ArrayIterator it = new ArrayIterator( array );
//		System.out.println(it.hasNext( ));
//		System.out.println(it.next());
//		it.remove();
		System.out.println(it.toString());
////		
//		String [] array = { "apple", "banana", null, "carrot" };
//		ArrayIterator<String> iter = new ArrayIterator( array );
//		System.out.println(iter.next( )); // returns "apple" User is not required to call hasNext.
//		iter.remove();
//		System.out.println(iter.hasNext( )); // true
//		System.out.println(iter.hasNext( )); // true again
//		System.out.println(iter.hasNext( )); // true again User can call hasNext many times
//		System.out.println(iter.getElement());
//		System.out.println(iter.next( )); // returns "banana"
//		iter.remove();
//		System.out.println(iter.next( )); // returns "carrot" (skip over null element)
//		iter.remove();
////		System.out.println(iter.toString());
//		System.out.println(iter.hasNext()); // false
//		System.out.println(iter.toString());
//		System.out.println(iter.next( )); // throws NoSuchElementException
	}
}
